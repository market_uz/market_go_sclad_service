CREATE TABLE IF NOT EXISTS "comings" (
    "id" UUID PRIMARY KEY,
    "nomer_prixoda" VARCHAR(50) NOT NULL,
    "branch_id" UUID NOT NULL,
    "courier_id" UUID NOT NULL,
    "order_date" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    -- 1 - IN_PROCESS, 2 - FINISHED
    "status" SMALLINT DEFAULT 1,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP 
);
