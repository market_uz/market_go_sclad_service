CREATE TABLE IF NOT EXISTS "ostatoks" (
    "id" UUID PRIMARY KEY,
    "nomer_ostatok" VARCHAR(50) NOT NULL,
    "branch_id" UUID NOT NULL,
    "category_id" UUID NOT NULL,
    "product_name" VARCHAR(50) NOT NULL,
    "count" INT NOT NULL,
    "barcode" VARCHAR(50) NOT NULL,
    "coming_price" NUMERIC NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP 
);
