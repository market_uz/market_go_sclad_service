CREATE TABLE IF NOT EXISTS "coming_products" (
    "id" UUID PRIMARY KEY,
    "coming_id" UUID REFERENCES comings(id),
    "category_id" UUID NOT NULL,
    "product_name" VARCHAR(50) NOT NULL,
    "count" INT NOT NULL,
    "barcode" VARCHAR (10) NOT NULL,
    "coming_price" NUMERIC NOT NULL,
    "create_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);