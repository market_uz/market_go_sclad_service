// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.12.4
// source: coming.proto

package sklad_service

import (
	_struct "github.com/golang/protobuf/ptypes/struct"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type ComingPrimaryKey struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *ComingPrimaryKey) Reset() {
	*x = ComingPrimaryKey{}
	if protoimpl.UnsafeEnabled {
		mi := &file_coming_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ComingPrimaryKey) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ComingPrimaryKey) ProtoMessage() {}

func (x *ComingPrimaryKey) ProtoReflect() protoreflect.Message {
	mi := &file_coming_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ComingPrimaryKey.ProtoReflect.Descriptor instead.
func (*ComingPrimaryKey) Descriptor() ([]byte, []int) {
	return file_coming_proto_rawDescGZIP(), []int{0}
}

func (x *ComingPrimaryKey) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type Coming struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id           string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	NomerPrixoda string `protobuf:"bytes,2,opt,name=nomer_prixoda,json=nomerPrixoda,proto3" json:"nomer_prixoda,omitempty"`
	BranchId     string `protobuf:"bytes,3,opt,name=branch_id,json=branchId,proto3" json:"branch_id,omitempty"`
	CourierId    string `protobuf:"bytes,4,opt,name=courier_id,json=courierId,proto3" json:"courier_id,omitempty"`
	OrderDate    string `protobuf:"bytes,5,opt,name=order_date,json=orderDate,proto3" json:"order_date,omitempty"`
	Status       int32  `protobuf:"varint,6,opt,name=status,proto3" json:"status,omitempty"`
	CreatedAt    string `protobuf:"bytes,7,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt    string `protobuf:"bytes,8,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
}

func (x *Coming) Reset() {
	*x = Coming{}
	if protoimpl.UnsafeEnabled {
		mi := &file_coming_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Coming) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Coming) ProtoMessage() {}

func (x *Coming) ProtoReflect() protoreflect.Message {
	mi := &file_coming_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Coming.ProtoReflect.Descriptor instead.
func (*Coming) Descriptor() ([]byte, []int) {
	return file_coming_proto_rawDescGZIP(), []int{1}
}

func (x *Coming) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *Coming) GetNomerPrixoda() string {
	if x != nil {
		return x.NomerPrixoda
	}
	return ""
}

func (x *Coming) GetBranchId() string {
	if x != nil {
		return x.BranchId
	}
	return ""
}

func (x *Coming) GetCourierId() string {
	if x != nil {
		return x.CourierId
	}
	return ""
}

func (x *Coming) GetOrderDate() string {
	if x != nil {
		return x.OrderDate
	}
	return ""
}

func (x *Coming) GetStatus() int32 {
	if x != nil {
		return x.Status
	}
	return 0
}

func (x *Coming) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *Coming) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

type CreateComing struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	BranchId  string `protobuf:"bytes,1,opt,name=branch_id,json=branchId,proto3" json:"branch_id,omitempty"`
	CourierId string `protobuf:"bytes,2,opt,name=courier_id,json=courierId,proto3" json:"courier_id,omitempty"`
	OrderDate string `protobuf:"bytes,3,opt,name=order_date,json=orderDate,proto3" json:"order_date,omitempty"`
}

func (x *CreateComing) Reset() {
	*x = CreateComing{}
	if protoimpl.UnsafeEnabled {
		mi := &file_coming_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateComing) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateComing) ProtoMessage() {}

func (x *CreateComing) ProtoReflect() protoreflect.Message {
	mi := &file_coming_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateComing.ProtoReflect.Descriptor instead.
func (*CreateComing) Descriptor() ([]byte, []int) {
	return file_coming_proto_rawDescGZIP(), []int{2}
}

func (x *CreateComing) GetBranchId() string {
	if x != nil {
		return x.BranchId
	}
	return ""
}

func (x *CreateComing) GetCourierId() string {
	if x != nil {
		return x.CourierId
	}
	return ""
}

func (x *CreateComing) GetOrderDate() string {
	if x != nil {
		return x.OrderDate
	}
	return ""
}

type UpdateComing struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id        string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	BranchId  string `protobuf:"bytes,2,opt,name=branch_id,json=branchId,proto3" json:"branch_id,omitempty"`
	CourierId string `protobuf:"bytes,3,opt,name=courier_id,json=courierId,proto3" json:"courier_id,omitempty"`
	OrderDate string `protobuf:"bytes,4,opt,name=order_date,json=orderDate,proto3" json:"order_date,omitempty"`
}

func (x *UpdateComing) Reset() {
	*x = UpdateComing{}
	if protoimpl.UnsafeEnabled {
		mi := &file_coming_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateComing) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateComing) ProtoMessage() {}

func (x *UpdateComing) ProtoReflect() protoreflect.Message {
	mi := &file_coming_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateComing.ProtoReflect.Descriptor instead.
func (*UpdateComing) Descriptor() ([]byte, []int) {
	return file_coming_proto_rawDescGZIP(), []int{3}
}

func (x *UpdateComing) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *UpdateComing) GetBranchId() string {
	if x != nil {
		return x.BranchId
	}
	return ""
}

func (x *UpdateComing) GetCourierId() string {
	if x != nil {
		return x.CourierId
	}
	return ""
}

func (x *UpdateComing) GetOrderDate() string {
	if x != nil {
		return x.OrderDate
	}
	return ""
}

type UpdatePatchComing struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id     string          `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Fields *_struct.Struct `protobuf:"bytes,2,opt,name=fields,proto3" json:"fields,omitempty"`
}

func (x *UpdatePatchComing) Reset() {
	*x = UpdatePatchComing{}
	if protoimpl.UnsafeEnabled {
		mi := &file_coming_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdatePatchComing) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdatePatchComing) ProtoMessage() {}

func (x *UpdatePatchComing) ProtoReflect() protoreflect.Message {
	mi := &file_coming_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdatePatchComing.ProtoReflect.Descriptor instead.
func (*UpdatePatchComing) Descriptor() ([]byte, []int) {
	return file_coming_proto_rawDescGZIP(), []int{4}
}

func (x *UpdatePatchComing) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *UpdatePatchComing) GetFields() *_struct.Struct {
	if x != nil {
		return x.Fields
	}
	return nil
}

type GetListComingRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Offset int64  `protobuf:"varint,1,opt,name=offset,proto3" json:"offset,omitempty"`
	Limit  int64  `protobuf:"varint,2,opt,name=limit,proto3" json:"limit,omitempty"`
	Search string `protobuf:"bytes,3,opt,name=search,proto3" json:"search,omitempty"`
}

func (x *GetListComingRequest) Reset() {
	*x = GetListComingRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_coming_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetListComingRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetListComingRequest) ProtoMessage() {}

func (x *GetListComingRequest) ProtoReflect() protoreflect.Message {
	mi := &file_coming_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetListComingRequest.ProtoReflect.Descriptor instead.
func (*GetListComingRequest) Descriptor() ([]byte, []int) {
	return file_coming_proto_rawDescGZIP(), []int{5}
}

func (x *GetListComingRequest) GetOffset() int64 {
	if x != nil {
		return x.Offset
	}
	return 0
}

func (x *GetListComingRequest) GetLimit() int64 {
	if x != nil {
		return x.Limit
	}
	return 0
}

func (x *GetListComingRequest) GetSearch() string {
	if x != nil {
		return x.Search
	}
	return ""
}

type GetListComingResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Count   int64     `protobuf:"varint,1,opt,name=count,proto3" json:"count,omitempty"`
	Comings []*Coming `protobuf:"bytes,2,rep,name=comings,proto3" json:"comings,omitempty"`
}

func (x *GetListComingResponse) Reset() {
	*x = GetListComingResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_coming_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetListComingResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetListComingResponse) ProtoMessage() {}

func (x *GetListComingResponse) ProtoReflect() protoreflect.Message {
	mi := &file_coming_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetListComingResponse.ProtoReflect.Descriptor instead.
func (*GetListComingResponse) Descriptor() ([]byte, []int) {
	return file_coming_proto_rawDescGZIP(), []int{6}
}

func (x *GetListComingResponse) GetCount() int64 {
	if x != nil {
		return x.Count
	}
	return 0
}

func (x *GetListComingResponse) GetComings() []*Coming {
	if x != nil {
		return x.Comings
	}
	return nil
}

var File_coming_proto protoreflect.FileDescriptor

var file_coming_proto_rawDesc = []byte{
	0x0a, 0x0c, 0x63, 0x6f, 0x6d, 0x69, 0x6e, 0x67, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0d,
	0x73, 0x6b, 0x6c, 0x61, 0x64, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x1a, 0x1c, 0x67,
	0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x73,
	0x74, 0x72, 0x75, 0x63, 0x74, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x22, 0x0a, 0x10, 0x43,
	0x6f, 0x6d, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b, 0x65, 0x79, 0x12,
	0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x22,
	0xee, 0x01, 0x0a, 0x06, 0x43, 0x6f, 0x6d, 0x69, 0x6e, 0x67, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x23, 0x0a, 0x0d, 0x6e, 0x6f,
	0x6d, 0x65, 0x72, 0x5f, 0x70, 0x72, 0x69, 0x78, 0x6f, 0x64, 0x61, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x0c, 0x6e, 0x6f, 0x6d, 0x65, 0x72, 0x50, 0x72, 0x69, 0x78, 0x6f, 0x64, 0x61, 0x12,
	0x1b, 0x0a, 0x09, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x08, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x49, 0x64, 0x12, 0x1d, 0x0a, 0x0a,
	0x63, 0x6f, 0x75, 0x72, 0x69, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x09, 0x63, 0x6f, 0x75, 0x72, 0x69, 0x65, 0x72, 0x49, 0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x6f,
	0x72, 0x64, 0x65, 0x72, 0x5f, 0x64, 0x61, 0x74, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x09, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x44, 0x61, 0x74, 0x65, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x74,
	0x61, 0x74, 0x75, 0x73, 0x18, 0x06, 0x20, 0x01, 0x28, 0x05, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74,
	0x75, 0x73, 0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74,
	0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x41,
	0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18,
	0x08, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74,
	0x22, 0x69, 0x0a, 0x0c, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x43, 0x6f, 0x6d, 0x69, 0x6e, 0x67,
	0x12, 0x1b, 0x0a, 0x09, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x08, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x49, 0x64, 0x12, 0x1d, 0x0a,
	0x0a, 0x63, 0x6f, 0x75, 0x72, 0x69, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x09, 0x63, 0x6f, 0x75, 0x72, 0x69, 0x65, 0x72, 0x49, 0x64, 0x12, 0x1d, 0x0a, 0x0a,
	0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x64, 0x61, 0x74, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x09, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x44, 0x61, 0x74, 0x65, 0x22, 0x79, 0x0a, 0x0c, 0x55,
	0x70, 0x64, 0x61, 0x74, 0x65, 0x43, 0x6f, 0x6d, 0x69, 0x6e, 0x67, 0x12, 0x0e, 0x0a, 0x02, 0x69,
	0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x1b, 0x0a, 0x09, 0x62,
	0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08,
	0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x49, 0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x6f, 0x75, 0x72,
	0x69, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x6f,
	0x75, 0x72, 0x69, 0x65, 0x72, 0x49, 0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x6f, 0x72, 0x64, 0x65, 0x72,
	0x5f, 0x64, 0x61, 0x74, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x6f, 0x72, 0x64,
	0x65, 0x72, 0x44, 0x61, 0x74, 0x65, 0x22, 0x54, 0x0a, 0x11, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65,
	0x50, 0x61, 0x74, 0x63, 0x68, 0x43, 0x6f, 0x6d, 0x69, 0x6e, 0x67, 0x12, 0x0e, 0x0a, 0x02, 0x69,
	0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x2f, 0x0a, 0x06, 0x66,
	0x69, 0x65, 0x6c, 0x64, 0x73, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x17, 0x2e, 0x67, 0x6f,
	0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x53, 0x74,
	0x72, 0x75, 0x63, 0x74, 0x52, 0x06, 0x66, 0x69, 0x65, 0x6c, 0x64, 0x73, 0x22, 0x5c, 0x0a, 0x14,
	0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x43, 0x6f, 0x6d, 0x69, 0x6e, 0x67, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x12, 0x14, 0x0a, 0x05,
	0x6c, 0x69, 0x6d, 0x69, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x6c, 0x69, 0x6d,
	0x69, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x18, 0x03, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x06, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x22, 0x5e, 0x0a, 0x15, 0x47, 0x65,
	0x74, 0x4c, 0x69, 0x73, 0x74, 0x43, 0x6f, 0x6d, 0x69, 0x6e, 0x67, 0x52, 0x65, 0x73, 0x70, 0x6f,
	0x6e, 0x73, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x03, 0x52, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x2f, 0x0a, 0x07, 0x63, 0x6f, 0x6d,
	0x69, 0x6e, 0x67, 0x73, 0x18, 0x02, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x15, 0x2e, 0x73, 0x6b, 0x6c,
	0x61, 0x64, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x43, 0x6f, 0x6d, 0x69, 0x6e,
	0x67, 0x52, 0x07, 0x63, 0x6f, 0x6d, 0x69, 0x6e, 0x67, 0x73, 0x42, 0x18, 0x5a, 0x16, 0x67, 0x65,
	0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x73, 0x6b, 0x6c, 0x61, 0x64, 0x5f, 0x73, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_coming_proto_rawDescOnce sync.Once
	file_coming_proto_rawDescData = file_coming_proto_rawDesc
)

func file_coming_proto_rawDescGZIP() []byte {
	file_coming_proto_rawDescOnce.Do(func() {
		file_coming_proto_rawDescData = protoimpl.X.CompressGZIP(file_coming_proto_rawDescData)
	})
	return file_coming_proto_rawDescData
}

var file_coming_proto_msgTypes = make([]protoimpl.MessageInfo, 7)
var file_coming_proto_goTypes = []interface{}{
	(*ComingPrimaryKey)(nil),      // 0: sklad_service.ComingPrimaryKey
	(*Coming)(nil),                // 1: sklad_service.Coming
	(*CreateComing)(nil),          // 2: sklad_service.CreateComing
	(*UpdateComing)(nil),          // 3: sklad_service.UpdateComing
	(*UpdatePatchComing)(nil),     // 4: sklad_service.UpdatePatchComing
	(*GetListComingRequest)(nil),  // 5: sklad_service.GetListComingRequest
	(*GetListComingResponse)(nil), // 6: sklad_service.GetListComingResponse
	(*_struct.Struct)(nil),        // 7: google.protobuf.Struct
}
var file_coming_proto_depIdxs = []int32{
	7, // 0: sklad_service.UpdatePatchComing.fields:type_name -> google.protobuf.Struct
	1, // 1: sklad_service.GetListComingResponse.comings:type_name -> sklad_service.Coming
	2, // [2:2] is the sub-list for method output_type
	2, // [2:2] is the sub-list for method input_type
	2, // [2:2] is the sub-list for extension type_name
	2, // [2:2] is the sub-list for extension extendee
	0, // [0:2] is the sub-list for field type_name
}

func init() { file_coming_proto_init() }
func file_coming_proto_init() {
	if File_coming_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_coming_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ComingPrimaryKey); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_coming_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Coming); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_coming_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateComing); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_coming_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateComing); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_coming_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdatePatchComing); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_coming_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetListComingRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_coming_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetListComingResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_coming_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   7,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_coming_proto_goTypes,
		DependencyIndexes: file_coming_proto_depIdxs,
		MessageInfos:      file_coming_proto_msgTypes,
	}.Build()
	File_coming_proto = out.File
	file_coming_proto_rawDesc = nil
	file_coming_proto_goTypes = nil
	file_coming_proto_depIdxs = nil
}
