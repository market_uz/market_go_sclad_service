package service

import (
	"app/config"
	"app/genproto/sklad_service"
	"app/grpc/client"
	"app/models"
	"app/pkg/logger"
	"app/storage"
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type OstatokService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*sklad_service.UnimplementedOstatokServiceServer
}

func NewOstatokService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *OstatokService {
	return &OstatokService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *OstatokService) Create(ctx context.Context, req *sklad_service.CreateOstatok) (resp *sklad_service.Ostatok, err error) {

	i.log.Info("---CreateOstatok------>", logger.Any("req", req))

	pKey, err := i.strg.Ostatok().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateOstatok->Ostatok->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Ostatok().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyOstatok->Ostatok->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *OstatokService) GetByID(ctx context.Context, req *sklad_service.OstatokPrimaryKey) (resp *sklad_service.Ostatok, err error) {

	i.log.Info("---GetOstatokByID------>", logger.Any("req", req))

	resp, err = i.strg.Ostatok().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetOstatokByID->Ostatok->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *OstatokService) GetList(ctx context.Context, req *sklad_service.GetListOstatokRequest) (resp *sklad_service.GetListOstatokResponse, err error) {

	i.log.Info("---GetOstatoks------>", logger.Any("req", req))

	resp, err = i.strg.Ostatok().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetOstatoks->Ostatok->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *OstatokService) Update(ctx context.Context, req *sklad_service.UpdateOstatok) (resp *sklad_service.Ostatok, err error) {

	i.log.Info("---UpdateOstatok------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Ostatok().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateOstatok--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Ostatok().GetByPKey(ctx, &sklad_service.OstatokPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetOstatok->Ostatok->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *OstatokService) UpdatePatch(ctx context.Context, req *sklad_service.UpdatePatchOstatok) (resp *sklad_service.Ostatok, err error) {

	i.log.Info("---UpdatePatchOstatok------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Ostatok().UpdatePatch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchOstatok--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Ostatok().GetByPKey(ctx, &sklad_service.OstatokPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetOstatok->Ostatok->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *OstatokService) Delete(ctx context.Context, req *sklad_service.OstatokPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteOstatok------>", logger.Any("req", req))

	err = i.strg.Ostatok().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteOstatok->Ostatok->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
