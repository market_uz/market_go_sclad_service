package service

import (
	"app/config"
	"app/genproto/sklad_service"
	"app/grpc/client"
	"app/models"
	"app/pkg/logger"
	"app/storage"
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ComingService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*sklad_service.UnimplementedComingServiceServer
}

func NewComingService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *ComingService {
	return &ComingService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *ComingService) Create(ctx context.Context, req *sklad_service.CreateComing) (resp *sklad_service.Coming, err error) {

	i.log.Info("---CreateComing------>", logger.Any("req", req))

	pKey, err := i.strg.Coming().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateComing->Coming->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Coming().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyComing->Coming->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ComingService) GetByID(ctx context.Context, req *sklad_service.ComingPrimaryKey) (resp *sklad_service.Coming, err error) {

	i.log.Info("---GetComingByID------>", logger.Any("req", req))

	resp, err = i.strg.Coming().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetComingByID->Coming->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ComingService) GetList(ctx context.Context, req *sklad_service.GetListComingRequest) (resp *sklad_service.GetListComingResponse, err error) {

	i.log.Info("---GetComings------>", logger.Any("req", req))

	resp, err = i.strg.Coming().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetComings->Coming->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ComingService) Update(ctx context.Context, req *sklad_service.UpdateComing) (resp *sklad_service.Coming, err error) {

	i.log.Info("---UpdateComing------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Coming().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateComing--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Coming().GetByPKey(ctx, &sklad_service.ComingPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetComing->Coming->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ComingService) UpdatePatch(ctx context.Context, req *sklad_service.UpdatePatchComing) (resp *sklad_service.Coming, err error) {

	i.log.Info("---UpdatePatchComing------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Coming().UpdatePatch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchComing--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Coming().GetByPKey(ctx, &sklad_service.ComingPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetComing->Coming->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ComingService) Delete(ctx context.Context, req *sklad_service.ComingPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteComing------>", logger.Any("req", req))

	err = i.strg.Coming().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteComing->Coming->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
