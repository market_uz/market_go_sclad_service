package service

import (
	"app/config"
	"app/genproto/sklad_service"
	"app/grpc/client"
	"app/models"
	"app/pkg/logger"
	"app/storage"
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ComingProductService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*sklad_service.UnimplementedComingProductServiceServer
}

func NewComingProductService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *ComingProductService {
	return &ComingProductService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *ComingProductService) Create(ctx context.Context, req *sklad_service.CreateComingProduct) (resp *sklad_service.ComingProduct, err error) {

	i.log.Info("---CreateCoingProduct------>", logger.Any("req", req))

	pKey, err := i.strg.ComingProduct().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateComingProduct->ComingProduct->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.ComingProduct().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyComingProduct->ComingProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ComingProductService) GetByID(ctx context.Context, req *sklad_service.ComingProductPrimaryKey) (resp *sklad_service.ComingProduct, err error) {

	i.log.Info("---GetComingProductByID------>", logger.Any("req", req))

	resp, err = i.strg.ComingProduct().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetComingProductByID->ComingProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ComingProductService) GetList(ctx context.Context, req *sklad_service.GetListComingProductRequest) (resp *sklad_service.GetListComingProductResponse, err error) {

	i.log.Info("---GetComingProducts------>", logger.Any("req", req))

	resp, err = i.strg.ComingProduct().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetComingProducts->ComingProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ComingProductService) Update(ctx context.Context, req *sklad_service.UpdateComingProduct) (resp *sklad_service.ComingProduct, err error) {

	i.log.Info("---UpdateComingProduct------>", logger.Any("req", req))

	rowsAffected, err := i.strg.ComingProduct().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateComingProduct--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.ComingProduct().GetByPKey(ctx, &sklad_service.ComingProductPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetComingProduct->ComingProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ComingProductService) UpdatePatch(ctx context.Context, req *sklad_service.UpdatePatchComingProduct) (resp *sklad_service.ComingProduct, err error) {

	i.log.Info("---UpdatePatchComingProduct------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.ComingProduct().UpdatePatch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchComingProduct--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.ComingProduct().GetByPKey(ctx, &sklad_service.ComingProductPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetComingProduct->ComingProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ComingProductService) Delete(ctx context.Context, req *sklad_service.ComingProductPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteComingProduct------>", logger.Any("req", req))

	err = i.strg.ComingProduct().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteComingProduct->ComingProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}