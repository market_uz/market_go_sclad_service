package grpc

import (
	"app/config"
	"app/genproto/sklad_service"
	"app/grpc/client"
	"app/grpc/service"
	"app/pkg/logger"
	"app/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	sklad_service.RegisterComingProductServiceServer(grpcServer, service.NewComingProductService(cfg, log, strg, srvc))
	sklad_service.RegisterComingServiceServer(grpcServer, service.NewComingService(cfg, log, strg, srvc))
	sklad_service.RegisterOstatokServiceServer(grpcServer, service.NewOstatokService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
