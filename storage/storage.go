package storage

import (
	"app/genproto/sklad_service"
	"app/models"
	"context"
)

type StorageI interface {
	CloseDB()
	ComingProduct() ComingProductI
	Coming() ComingI
	Ostatok() OstatokI
}

type ComingProductI interface {
	Create(ctx context.Context, req *sklad_service.CreateComingProduct) (resp *sklad_service.ComingProductPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *sklad_service.ComingProductPrimaryKey) (resp *sklad_service.ComingProduct, err error)
	GetAll(ctx context.Context, req *sklad_service.GetListComingProductRequest) (resp *sklad_service.GetListComingProductResponse, err error)
	Update(ctx context.Context, req *sklad_service.UpdateComingProduct) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *sklad_service.ComingProductPrimaryKey) error
}

type ComingI interface {
	Create(ctx context.Context, req *sklad_service.CreateComing) (resp *sklad_service.ComingPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *sklad_service.ComingPrimaryKey) (resp *sklad_service.Coming, err error)
	GetAll(ctx context.Context, req *sklad_service.GetListComingRequest) (resp *sklad_service.GetListComingResponse, err error)
	Update(ctx context.Context, req *sklad_service.UpdateComing) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *sklad_service.ComingPrimaryKey) error
}

type OstatokI interface {
	Create(ctx context.Context, req *sklad_service.CreateOstatok) (resp *sklad_service.OstatokPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *sklad_service.OstatokPrimaryKey) (resp *sklad_service.Ostatok, err error)
	GetAll(ctx context.Context, req *sklad_service.GetListOstatokRequest) (resp *sklad_service.GetListOstatokResponse, err error)
	Update(ctx context.Context, req *sklad_service.UpdateOstatok) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *sklad_service.OstatokPrimaryKey) error
}
