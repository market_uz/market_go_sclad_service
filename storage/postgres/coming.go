package postgres

import (
	"app/genproto/sklad_service"
	"app/models"
	"app/pkg/helper"
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type comingRepo struct {
	db *pgxpool.Pool
}

func NewComingRepo(db *pgxpool.Pool) *comingRepo {
	return &comingRepo{
		db: db,
	}
}

func (c *comingRepo) Create(ctx context.Context, req *sklad_service.CreateComing) (resp *sklad_service.ComingPrimaryKey, err error) {
	var id = uuid.New().String()

	nomer_prixoda := helper.GenerateCode()

	query := `
		INSERT INTO "comings" (
			id, 
			nomer_prixoda,
			branch_id,
			courier_id,
			updated_at
		) VALUES ($1, $2, $3, $4, NOW())
	`

	_, err = c.db.Exec(ctx,
		query,
		id,
		nomer_prixoda,
		req.BranchId,
		req.CourierId,
	)
	if err != nil {
		return nil, err
	}

	return &sklad_service.ComingPrimaryKey{Id: id}, nil
}

func (c *comingRepo) GetByPKey(ctx context.Context, req *sklad_service.ComingPrimaryKey) (resp *sklad_service.Coming, err error) {
	query := `
		SELECT
			id,
			nomer_prixoda,
			branch_id,
			courier_id,
			order_date,
			status,
			created_at,
			updated_at
		FROM "comings"
		WHERE id = $1
	`

	var (
		id            sql.NullString
		nomer_prixoda sql.NullString
		branch_id     sql.NullString
		courier_id    sql.NullString
		order_date    sql.NullString
		status        sql.NullInt32
		created_at    sql.NullString
		updated_at    sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&nomer_prixoda,
		&branch_id,
		&courier_id,
		&order_date,
		&status,
		&created_at,
		&updated_at,
	)
	if err != nil {
		return resp, err
	}

	resp = &sklad_service.Coming{
		Id:           id.String,
		NomerPrixoda: nomer_prixoda.String,
		BranchId:     branch_id.String,
		CourierId:    courier_id.String,
		OrderDate:    order_date.String,
		Status:       status.Int32,
		CreatedAt:    created_at.String,
		UpdatedAt:    updated_at.String,
	}

	return
}

func (c *comingRepo) GetAll(ctx context.Context, req *sklad_service.GetListComingRequest) (resp *sklad_service.GetListComingResponse, err error) {
	resp = &sklad_service.GetListComingResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE "
	)

	query = `
		SELECT 
			COUNT(*) OVER(),
			id,
			nomer_prixoda,
			branch_id,
			courier_id,
			TO_CHAR(order_date, 'YYYY-MM-DD HH24:MI:SS'),
			status,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "comings"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}
	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)

	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			id            sql.NullString
			nomer_prixoda sql.NullString
			branch_id     sql.NullString
			courier_id    sql.NullString
			order_date    sql.NullString
			status        sql.NullInt32
			created_at    sql.NullString
			updated_at    sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&nomer_prixoda,
			&branch_id,
			&courier_id,
			&order_date,
			&status,
			&created_at,
			&updated_at,
		)
		if err != nil {
			return resp, err
		}

		resp.Comings = append(resp.Comings, &sklad_service.Coming{
			Id:           id.String,
			NomerPrixoda: nomer_prixoda.String,
			BranchId:     branch_id.String,
			CourierId:    courier_id.String,
			OrderDate:    order_date.String,
			Status:       status.Int32,
			CreatedAt:    created_at.String,
			UpdatedAt:    updated_at.String,
		})
	}

	return
}

func (c *comingRepo) Update(ctx context.Context, req *sklad_service.UpdateComing) (rowsAffected int64, err error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"comings"
		SET
			branch_id = :branch_id,
			courier_id = :courier_id,
			updated_at = now()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":         req.GetId(),
		"branch_id":  req.GetBranchId(),
		"courier_id": req.GetCourierId(),
	}
	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *comingRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {
	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"comings"
		` + set + ` , updated_at = NOW()
			WHERE id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *comingRepo) Delete(ctx context.Context, req *sklad_service.ComingPrimaryKey) error {
	query := `DELETE FROM "comings" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)
	if err != nil {
		return err
	}
	return nil
}
