package postgres

import (
	"app/genproto/sklad_service"
	"app/models"
	"app/pkg/helper"
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type comingProductRepo struct {
	db *pgxpool.Pool
}

func NewComingProductRepo(db *pgxpool.Pool) *comingProductRepo {
	return &comingProductRepo{
		db: db,
	}
}

func (c *comingProductRepo) Create(ctx context.Context, req *sklad_service.CreateComingProduct) (resp *sklad_service.ComingProductPrimaryKey, err error) {
	var id = uuid.New().String()

	query := `
		INSERT INTO "coming_products" (
			id, 
			coming_id,
			category_id,
			product_name,
			count,
			barcode,
			coming_price,
			updated_at
		) VALUES ($1, $2, $3, $4, $5, $6, $7, NOW())
	`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.ComingId,
		req.CategoryId,
		req.ProductName,
		req.Count,
		req.Barcode,
		req.ComingPrice,
	)
	if err != nil {
		return nil, err
	}

	return &sklad_service.ComingProductPrimaryKey{Id: id}, nil
}

func (c *comingProductRepo) GetByPKey(ctx context.Context, req *sklad_service.ComingProductPrimaryKey) (resp *sklad_service.ComingProduct, err error) {
	query := `
		SELECT
			id,
			coming_id,
			category_id,
			product_name,
			count,
			barcode,
			coming_price,
			TO_CHAR(create_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "coming_products"
		WHERE id = $1
	`

	var (
		id           sql.NullString
		coming_id    sql.NullString
		category_id  sql.NullString
		product_name sql.NullString
		count        sql.NullInt32
		barcode      sql.NullString
		coming_price sql.NullFloat64
		created_at   sql.NullString
		updated_at   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&coming_id,
		&category_id,
		&product_name,
		&count,
		&barcode,
		&coming_price,
		&created_at,
		&updated_at,
	)
	if err != nil {
		return resp, err
	}

	resp = &sklad_service.ComingProduct{
		Id:          id.String,
		ComingId:    coming_id.String,
		CategoryId:  category_id.String,
		ProductName: product_name.String,
		Count:       count.Int32,
		Barcode:     barcode.String,
		ComingPrice: coming_price.Float64,
		CreatedAt:   created_at.String,
		UpdatedAt:   updated_at.String,
	}

	return
}

func (c *comingProductRepo) GetAll(ctx context.Context, req *sklad_service.GetListComingProductRequest) (resp *sklad_service.GetListComingProductResponse, err error) {
	resp = &sklad_service.GetListComingProductResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE "
	)

	query = `
		SELECT 
			COUNT(*) OVER(),
			id,
			coming_id,
			category_id,
			product_name,
			count,
			barcode,
			coming_price,
			TO_CHAR(create_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "coming_products"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}
	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)

	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			id           sql.NullString
			coming_id    sql.NullString
			category_id  sql.NullString
			product_name sql.NullString
			count        sql.NullInt32
			barcode      sql.NullString
			coming_price sql.NullFloat64
			created_at   sql.NullString
			updated_at   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&coming_id,
			&category_id,
			&product_name,
			&count,
			&barcode,
			&coming_price,
			&created_at,
			&updated_at,
		)
		if err != nil {
			return resp, err
		}

		resp.ComingProducts = append(resp.ComingProducts, &sklad_service.ComingProduct{
			Id:          id.String,
			ComingId:    coming_id.String,
			CategoryId:  category_id.String,
			ProductName: product_name.String,
			Count:       count.Int32,
			Barcode:     barcode.String,
			ComingPrice: coming_price.Float64,
			CreatedAt:   created_at.String,
			UpdatedAt:   updated_at.String,
		})
	}

	return
}

func (c *comingProductRepo) Update(ctx context.Context, req *sklad_service.UpdateComingProduct) (rowsAffected int64, err error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"coming_products"
		SET
			coming_id = :coming_id,
			category_id = :category_id,
			product_name = :product_name,
			count = :count,
			barcode = :barcode,
			coming_price = :coming_price,
			updated_at = now()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":           req.GetId(),
		"coming_id":    req.GetComingId(),
		"category_id":  req.GetCategoryId(),
		"product_name": req.GetProductName(),
		"count":        req.GetCount(),
		"barcode":      req.GetBarcode(),
		"coming_price": req.GetComingPrice(),
	}
	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *comingProductRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {
	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"coming_products"
		` + set + ` , updated_at = NOW()
			WHERE id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *comingProductRepo) Delete(ctx context.Context, req *sklad_service.ComingProductPrimaryKey) error {
	query := `DELETE FROM "coming_products" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)
	if err != nil {
		return err
	}
	return nil
}
