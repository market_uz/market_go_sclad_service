package postgres

import (
	"app/config"
	"app/storage"
	"context"
	"fmt"
	"log"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Store struct {
	db            *pgxpool.Pool
	comingProduct storage.ComingProductI
	coming        storage.ComingI
	ostatok       storage.OstatokI
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(fmt.Sprintf(
		"postgres://%s:%s@%s:%d/%s?sslmode=disable",
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDatabase,
	))
	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections

	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, err
}

func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) ComingProduct() storage.ComingProductI {
	if s.comingProduct == nil {
		s.comingProduct = NewComingProductRepo(s.db)
	}
	return s.comingProduct
}

func (s *Store) Coming() storage.ComingI {
	if s.coming == nil {
		s.coming = NewComingRepo(s.db)
	}
	return s.coming
}

func (s *Store) Ostatok() storage.OstatokI {
	if s.ostatok == nil {
		s.ostatok = NewOstatokRepo(s.db)
	}
	return s.ostatok
}

func (l *Store) Log(ctx context.Context, level pgx.LogLevel, msg string, data map[string]interface{}) {
	args := make([]interface{}, 0, len(data)+2) // making space for arguments + level + msg
	args = append(args, level, msg)
	for k, v := range data {
		args = append(args, fmt.Sprintf("%s=%v", k, v))
	}
	log.Println(args...)
}
